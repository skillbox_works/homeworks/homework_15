// HomeWork_15.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>

void out_num(int N, bool even)
{
    std::cout << (even ? "even" : "odd") << ":\n";
    int i = (even ? 0 : 1);
    while (i < N)
    {
        std::cout << i << '\n';
        i += 2;
    }
}

int main()
{
    int N = 10;
    bool b_even = true, b_odd = false;
    //
    std::cout << N << '\n';;
    out_num(N, b_even);
    out_num(N, b_odd); 

}
